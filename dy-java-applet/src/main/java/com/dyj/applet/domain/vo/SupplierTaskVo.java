package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-04-28 16:24
 **/
public class SupplierTaskVo extends BaseVo {

    /**
     * 任务id
     */
    private String task_id;

    /**
     * 结果链接
     */
    private String result_uri;

    /**
     * 任务状态	0 进行中 1 完成 2 失败
     */
    private Integer task_status;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getResult_uri() {
        return result_uri;
    }

    public void setResult_uri(String result_uri) {
        this.result_uri = result_uri;
    }

    public Integer getTask_status() {
        return task_status;
    }

    public void setTask_status(Integer task_status) {
        this.task_status = task_status;
    }
}
