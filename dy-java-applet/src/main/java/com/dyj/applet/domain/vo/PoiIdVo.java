package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-04-28 15:31
 **/
public class PoiIdVo extends BaseVo {

    /**
     * POI所在城市
     */
    private String city;
    /**
     * 纬度
     */
    private Float latitude;
    /**
     * 经度
     */
    private Float longitude;
    /**
     * 抖音POI ID
     */
    private String poi_id;
    /**
     * POI 名称
     */
    private String poi_name;
    /**
     * POI地址
     */
    private String address;
    /**
     * 高德POI ID
     */
    private String amap_id;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

    public String getPoi_name() {
        return poi_name;
    }

    public void setPoi_name(String poi_name) {
        this.poi_name = poi_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAmap_id() {
        return amap_id;
    }

    public void setAmap_id(String amap_id) {
        this.amap_id = amap_id;
    }
}
