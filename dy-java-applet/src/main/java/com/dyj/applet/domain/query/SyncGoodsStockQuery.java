package com.dyj.applet.domain.query;

import com.dyj.applet.domain.StockStruct;
import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-05-06 11:16
 **/
public class SyncGoodsStockQuery extends BaseQuery {

    private String product_id;
    private String out_id;
    private StockStruct stock;

    public static SyncGoodsStockQueryBuilder build() {
        return new SyncGoodsStockQueryBuilder();
    }

    public static final class SyncGoodsStockQueryBuilder {
        private String productId;
        private String outId;
        private StockStruct stock;
        private Integer tenantId;
        private String clientKey;

        public SyncGoodsStockQueryBuilder productId(String productId) {
            this.productId = productId;
            return this;
        }

        public SyncGoodsStockQueryBuilder outId(String outId) {
            this.outId = outId;
            return this;
        }

        public SyncGoodsStockQueryBuilder stock(StockStruct stock) {
            this.stock = stock;
            return this;
        }

        public SyncGoodsStockQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public SyncGoodsStockQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SyncGoodsStockQuery build() {
            SyncGoodsStockQuery syncGoodsStockQuery = new SyncGoodsStockQuery();
            syncGoodsStockQuery.setProduct_id(productId);
            syncGoodsStockQuery.setOut_id(outId);
            syncGoodsStockQuery.setStock(stock);
            syncGoodsStockQuery.setTenantId(tenantId);
            syncGoodsStockQuery.setClientKey(clientKey);
            return syncGoodsStockQuery;
        }

    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }

    public StockStruct getStock() {
        return stock;
    }

    public void setStock(StockStruct stock) {
        this.stock = stock;
    }
}
